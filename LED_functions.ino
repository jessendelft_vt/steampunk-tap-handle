
void sendHeartbeat(){
  /*Flashes quickly 2 times each 2 seconds.*/
  ++heartbeatCnt;
  if(heartbeatCnt>20)heartbeatCnt=1;
  switch(heartbeatCnt){
    case 1:
      digitalWrite(LED_BUILTIN, HIGH);
      break;
    case 3:
      digitalWrite(LED_BUILTIN, HIGH);
      break;
    default:
      digitalWrite(LED_BUILTIN, LOW);
      break;
  }
}

void burnPattern(){
  /*Creates a pattern that imitates burning candles*/
  for(uint8_t j=0;j<NUM_OF_TAPS;j++){         //Repeat function for each tap
    
    for(int16_t i=LEDS_PER_TAP-2;i>=0;i--){
      R[i+1+j*LEDS_PER_TAP]=R[i+j*LEDS_PER_TAP]/1.2;   
      G[i+1+j*LEDS_PER_TAP]=G[i+j*LEDS_PER_TAP]/1.75;
      B[i+1+j*LEDS_PER_TAP]=B[i+j*LEDS_PER_TAP]/2;
    }
  
    R[j*LEDS_PER_TAP]=191+random(64);
    G[j*LEDS_PER_TAP]=63+random(32);
    B[j*LEDS_PER_TAP]=0;
  
    for(uint8_t i=0;i<LEDS_PER_TAP;i++){
      F[i+j*LEDS_PER_TAP]=random(64);
      tR[i+j*LEDS_PER_TAP]=R[i+j*LEDS_PER_TAP]-F[i+j*LEDS_PER_TAP]-i*20;
      tG[i+j*LEDS_PER_TAP]=G[i+j*LEDS_PER_TAP]-F[i+j*LEDS_PER_TAP]-i*20;
      tB[i+j*LEDS_PER_TAP]=B[i+j*LEDS_PER_TAP]-F[i+j*LEDS_PER_TAP]-i*20;
      if(tR[i+j*LEDS_PER_TAP]<0)tR[i+j*LEDS_PER_TAP]=0;
      if(tG[i+j*LEDS_PER_TAP]<0)tG[i+j*LEDS_PER_TAP]=0;
      if(tB[i+j*LEDS_PER_TAP]<0)tB[i+j*LEDS_PER_TAP]=0;
      strip.setPixelColor(i+j*LEDS_PER_TAP,tR[i+j*LEDS_PER_TAP],tG[i+j*LEDS_PER_TAP],tB[i+j*LEDS_PER_TAP]);
    }
  }
  strip.show();
}

void fluxPattern(){
  /*Creates a pattern that imitates lightning rods*/
  for(uint8_t j=0;j<NUM_OF_TAPS;j++){         //Repeat function for each tap
    
    uint8_t sumF=0; 
    for(uint8_t i=0;i<5;i++){
      oldF[i+j*LEDS_PER_TAP]=F[i+j*LEDS_PER_TAP];
      sumF=F[i+j*LEDS_PER_TAP];
      F[i+j*LEDS_PER_TAP]=0;
    }
    if(random(100)>95)F[random(5)+j*LEDS_PER_TAP]=255;
    for(uint8_t i=0;i<5;i++){
      R[i+j*LEDS_PER_TAP]=0;
      G[i+j*LEDS_PER_TAP]=0;
      B[i+j*LEDS_PER_TAP]=15+random(92);
      if(sumF>0){
        if(oldF[0+j*LEDS_PER_TAP]==255){
          F[1]=64;
        }else if(oldF[1+j*LEDS_PER_TAP]==255){
          F[0]=64;
          F[2]=64;
        }else if(oldF[2+j*LEDS_PER_TAP]==255){
          F[1]=64;
          F[3]=64;
        }else if(oldF[3+j*LEDS_PER_TAP]==255){
          F[2]=64;
          F[4]=64;
        }else if(oldF[4+j*LEDS_PER_TAP]==255){
          F[3]=64;
        }
      }
      //F[i]=random(128);
      tR[i+j*LEDS_PER_TAP]=R[i+j*LEDS_PER_TAP]+F[i+j*LEDS_PER_TAP];
      tG[i+j*LEDS_PER_TAP]=G[i+j*LEDS_PER_TAP]+F[i+j*LEDS_PER_TAP];
      tB[i+j*LEDS_PER_TAP]=B[i+j*LEDS_PER_TAP]+F[i+j*LEDS_PER_TAP];
      if(tR[i+j*LEDS_PER_TAP]>255)tR[i+j*LEDS_PER_TAP]=255;
      if(tG[i+j*LEDS_PER_TAP]>255)tG[i+j*LEDS_PER_TAP]=255;
      if(tB[i+j*LEDS_PER_TAP]>255)tB[i+j*LEDS_PER_TAP]=255;
      strip.setPixelColor(i+j*LEDS_PER_TAP,tR[i+j*LEDS_PER_TAP],tG[i+j*LEDS_PER_TAP],tB[i+j*LEDS_PER_TAP]);
    }
  }
  strip.show();
}

void warpPattern(){
  /*Creates a pattern that imitates a warp engine*/
  if(++warpStage>9)warpStage=0;

  for(uint8_t j=0;j<NUM_OF_TAPS;j++){         //Repeat function for each tap
  
    for(uint8_t i=0;i<LEDS_PER_TAP-1;i++){
      R[i+j*LEDS_PER_TAP]=R[i+1+j*LEDS_PER_TAP];   
      G[i+j*LEDS_PER_TAP]=G[i+1+j*LEDS_PER_TAP];
      B[i+j*LEDS_PER_TAP]=B[i+1+j*LEDS_PER_TAP];
    }

    if(warpStage==0){R[(1+j)*LEDS_PER_TAP-1]=63;G[(1+j)*LEDS_PER_TAP-1]=63;B[(1+j)*LEDS_PER_TAP-1]=191;}
    if(warpStage==1){R[(1+j)*LEDS_PER_TAP-1]=31;G[(1+j)*LEDS_PER_TAP-1]=31;B[(1+j)*LEDS_PER_TAP-1]=127;}
    if(warpStage==2){R[(1+j)*LEDS_PER_TAP-1]=7;G[(1+j)*LEDS_PER_TAP-1]=7;B[(1+j)*LEDS_PER_TAP-1]=63;}
    if(warpStage==3){R[(1+j)*LEDS_PER_TAP-1]=3;G[(1+j)*LEDS_PER_TAP-1]=3;B[(1+j)*LEDS_PER_TAP-1]=15;}
    if(warpStage==4){R[(1+j)*LEDS_PER_TAP-1]=1;G[(1+j)*LEDS_PER_TAP-1]=1;B[(1+j)*LEDS_PER_TAP-1]=7;}
    if(warpStage==5){R[(1+j)*LEDS_PER_TAP-1]=0;G[(1+j)*LEDS_PER_TAP-1]=0;B[(1+j)*LEDS_PER_TAP-1]=3;}
    if(warpStage==6){R[(1+j)*LEDS_PER_TAP-1]=1;G[(1+j)*LEDS_PER_TAP-1]=1;B[(1+j)*LEDS_PER_TAP-1]=7;}
    if(warpStage==7){R[(1+j)*LEDS_PER_TAP-1]=3;G[(1+j)*LEDS_PER_TAP-1]=3;B[(1+j)*LEDS_PER_TAP-1]=15;}
    if(warpStage==8){R[(1+j)*LEDS_PER_TAP-1]=7;G[(1+j)*LEDS_PER_TAP-1]=7;B[(1+j)*LEDS_PER_TAP-1]=63;}
    if(warpStage==9){R[(1+j)*LEDS_PER_TAP-1]=31;G[(1+j)*LEDS_PER_TAP-1]=31;B[(1+j)*LEDS_PER_TAP-1]=127;}
  
    for(uint8_t i=0;i<LEDS_PER_TAP;i++){
      strip.setPixelColor(i+j*LEDS_PER_TAP,R[i+j*LEDS_PER_TAP],G[i+j*LEDS_PER_TAP],B[i+j*LEDS_PER_TAP]);
    }
  }
  strip.show();
}

void toxicPattern(){
  /*Creates a pattern that imitates a toxic bath*/
  warpStage+=pulsedir;
  if(warpStage==30 || warpStage==0)pulsedir=-pulsedir;

  for(uint8_t j=0;j<NUM_OF_TAPS;j++){         //Repeat function for each tap
    uint8_t Flicker=random(20);
    for(uint8_t i=0;i<LEDS_PER_TAP;i++){
      G[j*LEDS_PER_TAP+i]=warpStage*(i*i/4)-Flicker;
      if(G[j*LEDS_PER_TAP+i]<0)G[j*LEDS_PER_TAP+i]=0;
    }
    
    for(uint8_t i=0;i<LEDS_PER_TAP;i++){
      R[i+j*LEDS_PER_TAP]=0;
      B[i+j*LEDS_PER_TAP]=0;   
      strip.setPixelColor(i+j*LEDS_PER_TAP,R[i+j*LEDS_PER_TAP],G[i+j*LEDS_PER_TAP],B[i+j*LEDS_PER_TAP]);
    }
  }
  strip.show();
}

void beerPattern(){
  /*Creates a pattern that imitates beer with foaming bubbles*/
  for(uint8_t j=0;j<NUM_OF_TAPS;j++){         //Repeat function for each tap
    byte newBubble = 0;
    if(random(100)<10) newBubble = 1; //Creates a 10% chance on creating a new bubble in this tap

    for(uint8_t i=0;i<LEDS_PER_TAP-1;i++){ //Calculate new LED value in each tap
      R[i+j*LEDS_PER_TAP]=255;
      G[i+j*LEDS_PER_TAP]=200;
      B[i+j*LEDS_PER_TAP]=0;
      /*
      Bubbles[Position] = {Color,Speed,NextStepIn}
      Color 1: RGB[255,255,0]
      Color 2: RGB[255,255,100]
      Color 3: RGB[255,255,255]
      Speed: 1-3.
      NextStepIn: Countdown from Speed -> 0. If 0, move 1 up.
      */
      if(i==0&newBubble==1){                          //Create new bubble if newBubble =1
        Bubbles[i+j*LEDS_PER_TAP][1] = random(1,3);   //Set random color
        Bubbles[i+j*LEDS_PER_TAP][2] = random(1,3);   //Set random speed
        Bubbles[i+j*LEDS_PER_TAP][3] = Bubbles[i+j*LEDS_PER_TAP][2];  //Set speed startpoint
      }
      //Move bubbles up
      if(Bubbles[i+j*LEDS_PER_TAP][3] == 0){
        if(i<LEDS_PER_TAP-1) {
          Bubbles[i+1+j*LEDS_PER_TAP][1] = Bubbles[i+j*LEDS_PER_TAP][1];
          Bubbles[i+1+j*LEDS_PER_TAP][2] = Bubbles[i+j*LEDS_PER_TAP][2];
          Bubbles[i+1+j*LEDS_PER_TAP][3] = Bubbles[i+1+j*LEDS_PER_TAP][2];
        }
        
        Bubbles[i+j*LEDS_PER_TAP][1] = 0;
        Bubbles[i+j*LEDS_PER_TAP][2] = 0;
        Bubbles[i+j*LEDS_PER_TAP][3] = 0;
      }
      Bubbles[i+j*LEDS_PER_TAP][3] -= 1;  //Lower countdown
      
      //Set bubble color. when Color = 0, no bubble is present.
      switch(Bubbles[i+j*LEDS_PER_TAP][1]){
        case 1:
          R[i+j*LEDS_PER_TAP]=255;
          G[i+j*LEDS_PER_TAP]=255;
          B[i+j*LEDS_PER_TAP]=50;
          break;
        case 2:
          R[i+j*LEDS_PER_TAP]=255;
          G[i+j*LEDS_PER_TAP]=255;
          B[i+j*LEDS_PER_TAP]=100;
          break;
        case 3:
          R[i+j*LEDS_PER_TAP]=255;
          G[i+j*LEDS_PER_TAP]=255;
          B[i+j*LEDS_PER_TAP]=150;
          break;
      }
    }
    for(uint8_t i=0;i<LEDS_PER_TAP;i++){
      strip.setPixelColor(i+j*LEDS_PER_TAP,R[i+j*LEDS_PER_TAP],G[i+j*LEDS_PER_TAP],B[i+j*LEDS_PER_TAP]);
    }
  }
  strip.show();
}
