** Original file created by Beergineer/Fuzzie. **
** Edited by Jessendelft. **

*Feel free to use this software in your own Beer Tap Handles, or elsewhere if you find a good use.*

*And don't forget, keep **Beergineering**!*

---

## Changelog

1. Changed NeoPixel library to Dotstar library.
2. Changed if-if loop in main to switch.
3. Created an ISR to read the button when pushed. Also added internal pullup.
4. Added a new mode (#4) - Beer Bubbles.
5. Changed int to int8_t to save space.
6. Changed heartbeat from if-else to switch.
7. Optimized toxicPattern() for more than 5 leds in each tap.
8. Saves latest mode in EEPROM.
9. Moved the functions to a secondary file.
