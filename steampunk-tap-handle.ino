//#include <Adafruit_NeoPixel.h>  //Uncomment when using Neopixel
#include <Adafruit_DotStar.h>     //Comment out when using Neopixel
#include <SPI.h>
#include <EEPROM.h>

#define LEDS_PER_TAP 8
#define NUM_OF_TAPS 5

//#define DIGITALPIN_BUTTON A5       //Commented out since I'm using Interrupt pin 2
//#define DIGITALPIN_NEOPIXELPWM 3   //Uncomment when using Neopixel
#define INTERRUPTPIN 2
#define EEPROMADDR 0

/*
MODE = 0:   BURN / FURNACE PATTERN
MODE = 1:   LIGHTNING RODS PATTERN
MODE = 2:   WARP ENGINES PATTERN
MODE = 3:   TOXIC LAB / MAD SCIENTISTS PATTERN
MODE = 4;   BEER BUBBLES
*/
int8_t mode;

int R[LEDS_PER_TAP*NUM_OF_TAPS],G[LEDS_PER_TAP*NUM_OF_TAPS],B[LEDS_PER_TAP*NUM_OF_TAPS],F[LEDS_PER_TAP*NUM_OF_TAPS];
int tR[LEDS_PER_TAP*NUM_OF_TAPS],tG[LEDS_PER_TAP*NUM_OF_TAPS],tB[LEDS_PER_TAP*NUM_OF_TAPS];
int oldF[LEDS_PER_TAP*NUM_OF_TAPS];
int Bubbles[LEDS_PER_TAP*NUM_OF_TAPS][3];    //Bubbles[Position][Color,Speed,NextStepIn]
int8_t warpStage=0;
int8_t pulsedir=1;
int8_t heartbeatCnt=0;

//Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDS_PER_TAP*NUM_OF_TAPS, DIGITALPIN_NEOPIXELPWM, NEO_GRB + NEO_KHZ800); //Uncomment when using Neopixel
Adafruit_DotStar strip(LEDS_PER_TAP*NUM_OF_TAPS, DOTSTAR_BGR); //Used for SPI, SCK = D13, DO = D11  //Comment out when using Neopixel

void changeMode(){
  if(++mode>4)mode=0;
  EEPROM.update(EEPROMADDR, mode);
}

void setup() {
  mode = EEPROM.read(EEPROMADDR);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(INTERRUPTPIN, INPUT_PULLUP);        //Change INTERRUPTPIN to DIGITALPIN_BUTTON if that's what you're using
  attachInterrupt(digitalPinToInterrupt(INTERRUPTPIN), changeMode, FALLING);
  //pinMode(DIGITALPIN_NEOPIXELPWM, OUTPUT);  //Uncomment when using Neopixel
  strip.begin();
  randomSeed(analogRead(A0));
  for(uint8_t i=0;i<LEDS_PER_TAP*NUM_OF_TAPS;i++){
    strip.setPixelColor(i,0,0,0);
  }
  strip.show();
}

void loop() {
  switch(mode){
    case 0: burnPattern();  break;
    case 1: fluxPattern();  break;
    case 2: warpPattern();  break;
    case 3: toxicPattern(); break;
    case 4: beerPattern();  break;
  }
  sendHeartbeat();
  delay(100);
}
